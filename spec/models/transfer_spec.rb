require 'rails_helper'

RSpec.describe Transfer, type: :model do

  context "caching balances" do
    before(:each) do
      root = User.create({ name: "Иван", login: "root", password: "123123" } )
      user = User.create({ name: "Андрей", login: "user", password: "123123" })
      company = Company.create(name: "ООО РОГА И КОПЫТА", created_at: Time.now - 10.days)
      team = Company.create(name: "ООО ХОЛОДЕЦ", created_at: Time.now - 10.days)

      Transfer.create(received: user, value: 2500, bonuses: 100, created_at: Time.now - 10.days)
      Transfer.create(received: root, value: 2500, bonuses: 100, created_at: Time.now - 10.days)
      Transfer.create(received: company, value: 25000, bonuses: 500, created_at: Time.now - 10.days)
      Transfer.create(received: team, value: 25000, bonuses: 500, created_at: Time.now - 10.days)

      Transfer.create(sent: user, received: team, value: 500, created_at: Time.now - 10.days)
      Transfer.create(sent: user, received: team, value: 290, created_at: Time.now - 9.days)
      Transfer.create(sent: company, received: user, value: 1000, created_at: Time.now - 8.days)
      Transfer.create(sent: user, received: root, value: 200, created_at: Time.now - 7.days)
      Transfer.create(sent: team, received: company, value: 500, created_at: Time.now - 6.days)
      Transfer.create(sent: company, received: team, value: 200, created_at: Time.now)
    end

    it "caching balance" do
      root = User.first
      user = User.last
      company = Company.first
      team = Company.last

      expect(root.balance.to_f).to eq(2700.0)
      expect(user.balance.to_f).to eq(2510.0)
      expect(company.balance.to_f).to eq(24300.0)
      expect(team.balance.to_f).to eq(25490.0)
    end

  end

  context "ensures a positive balance" do
    before(:each) do
      root = User.create({ name: "Иван", login: "root", password: "123123" } )
      user = User.create({ name: "Андрей", login: "user", password: "123123" })
      company = Company.create(name: "ООО РОГА И КОПЫТА", created_at: Time.now - 10.days)
      team = Company.create(name: "ООО ХОЛОДЕЦ", created_at: Time.now - 10.days)

      Transfer.create(sent: user, received: root, value: 500, created_at: Time.now - 10.days)
      Transfer.create(sent: user, received: root, value: 290, created_at: Time.now - 9.days)
      Transfer.create(sent: root, received: user, value: 1000, created_at: Time.now - 8.days)
      Transfer.create(sent: user, received: root, value: 200, created_at: Time.now - 7.days)
      Transfer.create(sent: user, received: root, value: 500, created_at: Time.now - 6.days)
      Transfer.create(sent: user, received: company, value: 290, created_at: Time.now - 5.days)
      Transfer.create(sent: root, received: user, value: 1000, created_at: Time.now - 4.days)
      Transfer.create(sent: user, received: root, value: 200, created_at: Time.now - 3.days)
      Transfer.create(sent: root, received: user, value: 1000, created_at: Time.now - 2.days)
      Transfer.create(sent: user, received: root, value: 200, created_at: Time.now - 1.days)
      Transfer.create(sent: user, received: root, value: 200, created_at: Time.now)
      Transfer.create(sent: company, received: root, value: 200, created_at: Time.now)
    end
    it "caching balance" do
      root = User.first
      user = User.last
      company = Company.first
      team = Company.last

      expect(Transfer.count).to eq(0)
      expect(root.balance).to eq(0)
      expect(user.balance).to eq(0)
      expect(company.balance).to eq(0)
      expect(team.balance).to eq(0)
    end
  end

  context "ensures not deadlock" do
    before(:each) do
      root = User.create({ name: "Иван", login: "root", password: "123123" } )
      user = User.create({ name: "Андрей", login: "user", password: "123123" })

      Transfer.create(received: user, value: 2500, bonuses: 100, created_at: Time.now - 10.days)
      Transfer.create(received: root, value: 2500, bonuses: 100, created_at: Time.now - 10.days)

    end

    it "caching balance with deadlock" do
      root = User.first
      user = User.last

      thread1 = Thread.new do
        Transfer.create(sent: user, received: root, value: 290, created_at: Time.now - 9.days)
      end
      thread2 = Thread.new do
        Transfer.create(sent: root, received: user, value: 1000, created_at: Time.now - 8.days)
      end
      thread1.join
      thread2.join

      # expect(Transfer.count).to eq(2)
      expect(Transfer.count).to eq(4)
      expect(user.balance.to_f).to eq(3210)
      # expect(user.balance.to_f).to eq(2500)
      expect(root.balance.to_f).to eq(1790)
      # expect(root.balance.to_f).to eq(2500)

    end
  end
end
