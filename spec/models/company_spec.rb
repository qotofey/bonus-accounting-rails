require 'rails_helper'

RSpec.describe Company, type: :model do

  context "validation presence" do
    it "ensures name is present" do
      company = Company.new()
      expect(company.valid?).to eq(false)
    end

    it "ensures name is present" do
      company = Company.new(name: "ООО РОГА И КОПЫТА")
      expect(company.valid?).to eq(true)
    end
  end

  context "functions" do
    before(:each) do
      root = User.create({ name: "Иван", login: "root", password: "123123" } )
      user = User.create({ name: "Андрей", login: "user", password: "123123" })
      company = Company.create(name: "ООО РОГА И КОПЫТА", created_at: Time.now - 10.days)
      team = Company.create(name: "ООО ХОЛОДЕЦ", created_at: Time.now - 10.days)

      Transfer.create(received: user, value: 2500, bonuses: 100, created_at: Time.now - 10.days)
      Transfer.create(received: root, value: 2500, bonuses: 100, created_at: Time.now - 10.days)
      Transfer.create(received: company, value: 25000, bonuses: 500, created_at: Time.now - 10.days)
      Transfer.create(received: team, value: 25000, bonuses: 500, created_at: Time.now - 10.days)

      Transfer.create(sent: user, received: team, value: 500, created_at: Time.now - 10.days)
      Transfer.create(sent: user, received: team, value: 290, created_at: Time.now - 9.days)
      Transfer.create(sent: company, received: user, value: 1000, created_at: Time.now - 8.days)
      Transfer.create(sent: user, received: root, value: 200, created_at: Time.now - 7.days)
      Transfer.create(sent: team, received: company, value: 500, created_at: Time.now - 6.days)
      Transfer.create(sent: company, received: team, value: 200, created_at: Time.now)
    end

    it "get history" do
      expect(Company.first.get_history.size).to eq(2)      #company
      expect(Company.last.get_history.size).to eq(1)       #team
    end

    it "get transfers" do
      expect(Company.first.get_transfers.size).to eq(4)    #company
      expect(Company.last.get_transfers.size).to eq(5)     #team
    end

  end



end
