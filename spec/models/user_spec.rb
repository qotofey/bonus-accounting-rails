require 'rails_helper'

RSpec.describe User, type: :model do
  context "validation presence" do
    it "ensures name is present" do
      user = User.new(login: "root", password: "123123")
      expect(user.valid?).to eq(false)
    end

    it "ensures login is present" do
      user = User.new(name: "Иван", password: "123123")
      expect(user.valid?).to eq(false)
    end

    it "ensures password is present" do
      user = User.new(name: "Иван", login: "root")
      expect(user.valid?).to eq(false)
    end

    it "should be able to save user" do
      user = User.new(name: "Иван", login: "root", password: "123123")
      expect(user.save).to eq(true)
    end
  end

  context "validation uniqueness" do
    let(:params) { { name: "Иван", login: "root", password: "123123" } }
    before(:each) do
      User.create(params)
      User.create(params)
      User.create(params)
      User.create(params.merge(login: "ivan"))
      User.create(params.merge(login: "user"))
    end

    it "ensures login uniq" do
      expect(User.count).to eq(3)
    end

  end

  context "functions" do

    before(:each) do
      root = User.create({ name: "Иван", login: "root", password: "123123" } )
      user = User.create({ name: "Андрей", login: "user", password: "123123" })
      company = Company.create(name: "ООО РОГА И КОПЫТА", created_at: Time.now - 10.days)

      Transfer.create(received: user, value: 2500, bonuses: 100, created_at: Time.now - 10.days)
      Transfer.create(received: root, value: 2500, bonuses: 100, created_at: Time.now - 10.days)
      Transfer.create(received: company, value: 25000, bonuses: 500, created_at: Time.now - 10.days)

      Transfer.create(sent: user, received: root, value: 500, created_at: Time.now - 10.days)
      Transfer.create(sent: user, received: root, value: 290, created_at: Time.now - 9.days)
      Transfer.create(sent: root, received: user, value: 1000, created_at: Time.now - 8.days)
      Transfer.create(sent: user, received: root, value: 200, created_at: Time.now - 7.days)
      Transfer.create(sent: user, received: root, value: 500, created_at: Time.now - 6.days)
      Transfer.create(sent: company, received: root, value: 200, created_at: Time.now)
    end

    it "get history" do
      expect(User.first.get_history.size).to eq(1)      #root
      expect(User.last.get_history.size).to eq(4)       #user
    end

    it "get transfers" do
      expect(User.first.get_transfers.size).to eq(7)    #root
      expect(User.last.get_transfers.size).to eq(6)     #user
    end

  end


end
