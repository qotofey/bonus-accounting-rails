require 'rails_helper'

RSpec.describe "Companies", type: :request do
  before(:each) do
    Company.create(name: 'ООО РОГА И КОПЫТА')
    Company.create(name: 'ООО ХОЛОДЕЦ')
  end

  describe "GET /companies" do
    it "works! (now write some real specs)" do
      get companies_path
      expect(response).to have_http_status(200)
    end
  end

  describe "GET /companies/balances" do
    it "works! (now write some real specs)" do
      get balances_companies_path
      expect(response).to have_http_status(200)
    end
  end

  describe "GET /companies/1" do
    it "works! (now write some real specs)" do
      get company_path(Company.first)
      expect(response).to have_http_status(200)
    end
  end

  describe "POST /companies" do
    it "works! (now write some real specs)" do
      post companies_path({ company: { name: 'ООО БЛАЖЕННЫЙ СОН' } })
      expect(response).to have_http_status(201)
    end
  end

  describe "UPDATE /companies/1" do
    it "works! (now write some real specs)" do
      put company_path(Company.first, { company: { name: 'ООО ВОЛНА' } })
      expect(response).to have_http_status(200)
    end
  end

  describe "DELETE /companies/1" do
    it "works! (now write some real specs)" do
      delete company_path(Company.first)
      expect(response).to have_http_status(204)
    end
  end
end
