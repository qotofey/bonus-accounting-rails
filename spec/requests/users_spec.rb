require 'rails_helper'

RSpec.describe "Users", type: :request do
  before(:each) do
    User.create(name: 'Ivan', login: 'user', password: '123123')
    User.create(name: 'Ivan', login: 'root', password: '123123')
  end

  describe "GET /users" do
    it "works! (now write some real specs)" do
      get users_path
      expect(response).to have_http_status(200)
    end
  end

  describe "GET /users/balances" do
    it "works! (now write some real specs)" do
      get balances_users_path
      expect(response).to have_http_status(200)
    end
  end

  describe "GET /users/1" do
    it "works! (now write some real specs)" do
      get user_path(User.first)
      expect(response).to have_http_status(200)
    end
  end

  describe "POST /users" do
    it "works! (now write some real specs)" do
      post users_path({ user: { name: 'Ivan', login: 'user1', password: '123123' } })
      expect(response).to have_http_status(201)
    end
  end

  describe "UPDATE /users/1" do
    it "works! (now write some real specs)" do
      put user_path(User.first, { user: { name: 'Ivan', login: 'user2', password: '123123' } })
      expect(response).to have_http_status(200)
    end
  end

  describe "DELETE /users/1" do
    it "works! (now write some real specs)" do
      delete user_path(User.first)
      expect(response).to have_http_status(204)
    end
  end

end
