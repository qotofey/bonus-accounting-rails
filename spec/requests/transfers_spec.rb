require 'rails_helper'

RSpec.describe "Transfers", type: :request do
  before(:each) do
    root = User.create({ name: "Иван", login: "root", password: "123123" } )
    user = User.create({ name: "Андрей", login: "user", password: "123123" })
    company = Company.create(name: "ООО РОГА И КОПЫТА", created_at: Time.now - 10.days)
    team = Company.create(name: "ООО ХОЛОДЕЦ", created_at: Time.now - 10.days)

    Transfer.create(received: user, value: 2500, bonuses: 100, created_at: Time.now - 10.days)
    Transfer.create(received: root, value: 2500, bonuses: 100, created_at: Time.now - 10.days)
    Transfer.create(received: company, value: 25000, bonuses: 500, created_at: Time.now - 10.days)
    Transfer.create(received: team, value: 25000, bonuses: 500, created_at: Time.now - 10.days)

    Transfer.create(sent: user, received: team, value: 500, created_at: Time.now - 10.days)
    Transfer.create(sent: user, received: team, value: 290, created_at: Time.now - 9.days)
    Transfer.create(sent: company, received: user, value: 1000, created_at: Time.now - 8.days)
    Transfer.create(sent: user, received: root, value: 200, created_at: Time.now - 7.days)
    Transfer.create(sent: team, received: company, value: 500, created_at: Time.now - 6.days)
    Transfer.create(sent: company, received: team, value: 200, created_at: Time.now)
  end

  describe "GET /users/1/transfers" do
    it "works! (now write some real specs)" do
      get user_transfers_path(User.first)
      expect(response).to have_http_status(200)
    end
  end

  describe "GET /companies/1/transfers" do
    it "works! (now write some real specs)" do
      get company_transfers_path(Company.first)
      expect(response).to have_http_status(200)
    end
  end

  describe "GET /users/1/transfers/history" do
    it "works! (now write some real specs)" do
      get history_user_transfers_path(User.first)
      expect(response).to have_http_status(200)
    end
  end

  describe "GET /companies/1/transfers/history" do
    it "works! (now write some real specs)" do
      get history_company_transfers_path(Company.first)
      expect(response).to have_http_status(200)
    end
  end

  describe "POST /users/1/transfers/add" do
    it "works! (now write some real specs)" do
      post add_user_transfers_path(User.first, { transfer: { value: 417.0 }})
      expect(response).to have_http_status(201)
    end
  end

  describe "POST /companies/1/transfers/add" do
    it "works! (now write some real specs)" do
      post add_company_transfers_path(Company.first, { transfer: { value: 417.0 }})
      expect(response).to have_http_status(201)
    end
  end

end
