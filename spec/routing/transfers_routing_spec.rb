require "rails_helper"

RSpec.describe TransfersController, UsersController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/users/1/transfers").to route_to("transfers#index", :user_id => "1")
    end

    it "routes to #show" do
      expect(:get => "/transfers/1").to route_to("transfers#show", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/users/1/transfers").to route_to("transfers#create", :user_id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/transfers/1").to route_to("transfers#destroy", :id => "1")
    end
  end
end

RSpec.describe TransfersController, CompaniesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/companies/1/transfers").to route_to("transfers#index", :company_id => "1")
    end

    it "routes to #show" do
      expect(:get => "/transfers/1").to route_to("transfers#show", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/companies/1/transfers").to route_to("transfers#create", :company_id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/transfers/1").to route_to("transfers#destroy", :id => "1")
    end
  end
end