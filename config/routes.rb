Rails.application.routes.draw do

  resources :users, :companies do
    resources :transfers, except: [ :update, :destroy, :show ] do
      collection do
        post 'add', to: 'transfers#add_without_sender'
        get 'history', to: 'transfers#history'
      end
    end
    collection do
      get 'balances'
    end
  end

  # get 'users/:id/transactions',     to: 'users#history'
  # get 'companies/:id/transactions', to: 'companies#transactions'

  get     'transfers/:id',  to: 'transfers#show'
  delete  'transfers/:id',  to: 'transfers#destroy'

end
