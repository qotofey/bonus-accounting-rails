class TransfersController < ApplicationController
  before_action :set_transfer, only: [:show, :update, :destroy]

  # GET /transfers
  def index
    @parent = parent()
    @transfers = @parent.get_transfers

    render json: @transfers
  end

  def history
    @parent = parent()
    @transfers = @parent.get_history

    render json: @transfers, only: [ :name, :value, :created_at ]
  end

  # GET /transfers/1
  def show
    render json: @transfer
  end

  # POST /transfers
  def create
    @parent = parent()
    @transfer = @parent.sent_transfers.new(transfer_params)

    if @transfer.save
      render json: @transfer, status: :created
    else
      render json: @transfer.errors, status: :unprocessable_entity
    end
  end

  def add_without_sender
    @parent = parent()
    @transfer = @parent.received_transfers.create(transfer_params)
    if @transfer.valid?
      render json: @transfer, status: :created
    else
      render json: @transfer.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /transfers/1
  # def update
  #   if @transfer.update(transfer_params)
  #     render json: @transfer
  #   else
  #     render json: @transfer.errors, status: :unprocessable_entity
  #   end
  # end

  # DELETE /transfers/1
  def destroy
    @transfer.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transfer
      @transfer = Transfer.find(params[:id])
    end

    def parent
      return User.find(params[:user_id]) if params[:user_id]
      return Company.find(params[:company_id]) if params[:company_id]
    end

    # Only allow a trusted parameter "white list" through.
    def transfer_params
      params.require(:transfer).permit(:value, :bonuses, :received_id, :received_type)
    end
end
