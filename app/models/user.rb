class User < ApplicationRecord
  # validates :balance, presence: true, numericality: { greater_than_or_equal_to: 0.0 }
  # validates :bonuses, presence: true, numericality: { greater_than_or_equal_to: 0.0 }

  before_save { self.login = login.downcase }
  validates :name, presence: true, length: { maximum: 255 }
  VALID_LOGIN_REGEX = /(^[\w\d]+$)/
  validates :login, presence: true, length: { maximum: 255 }, format: { with: VALID_LOGIN_REGEX }, uniqueness: true

  has_secure_password

  has_many :sent_transfers, class_name: 'Transfer', as: :sent, dependent: :nullify
  has_many :received_transfers, class_name: 'Transfer', as: :received, dependent: :nullify

  def get_transfers
    Transfer.where(sent: self).or(Transfer.where(received: self))
  end

  def as_json(options = {})
    super(options.merge({ except: [:password_digest] }))
  end

  def get_history
    # " OR transfers.received_type = 'User' AND transfers.received_id = users.id"
    # можно добавить эту строку, если нам нужно видеть и приходящие транзакции
    self.get_transfers
        .joins("INNER JOIN users ON transfers.sent_type = 'User' AND transfers.sent_id = users.id")
        .select('transfers.id', 'users.name', 'transfers.value', 'transfers.created_at')
        .where("users.id = #{self.id}")
  end

end
