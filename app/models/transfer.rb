class Transfer < ApplicationRecord
  #validates :value, numericality: {greater_than_or_equal_to: 0.0}

  belongs_to :sent, polymorphic: true, counter_cache: :sent_transfers_count, optional: true
  belongs_to :received, polymorphic: true, counter_cache: :received_transfers_count

  # "кеширование" баланса
  before_create do

    transaction do
      ActiveRecord::Base.connection.execute("LOCK #{sent.class.table_name} IN ACCESS EXCLUSIVE MODE") if sent.present?
      ActiveRecord::Base.connection.execute("LOCK #{received.class.table_name} IN ACCESS EXCLUSIVE MODE") if received.present?

      self.sent&.balance -= value
      self.received&.balance += value

      self.sent&.save!
      self.received&.save!
    end

  end

  # "кеширование" баланса
  before_destroy do
    Transfer.transaction do
      ActiveRecord::Base.connection.execute("LOCK #{sent.class.table_name} IN ACCESS EXCLUSIVE MODE") if sent.present?
      ActiveRecord::Base.connection.execute("LOCK #{received.class.table_name} IN ACCESS EXCLUSIVE MODE") if received.present?

      self.sent&.balance += value
      self.received&.balance -= value

      self.sent&.save!
      self.received&.save!
    end
  end

  before_validation :ensures_different_users
  before_validation :ensures_a_positive_balance

  def ensures_different_users
    if self.sent == self.received
      errors.add(:errors, 'отправитель и получатель не должны быть одним пользователем')
      return false
    end
    return true
  end

  def ensures_a_positive_balance
    if self.sent.present? and self.sent.balance < self.value
      errors.add(:errors, 'у отправителя недостаточно средств')
      return false
    end
    if self.received.balance + self.value < 0
      errors.add(:errors, 'у пользователя недостаточно средств')
      return false
    end
    return true
  end

end
