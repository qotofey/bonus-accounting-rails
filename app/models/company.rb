class Company < ApplicationRecord
  validates :name, presence: true
  # validates :balance, presence: true, numericality: { greater_than_or_equal_to: 0.0 }
  # validates :bonuses, presence: true, numericality: { greater_than_or_equal_to: 0.0 }

  has_many :sent_transfers, class_name: 'Transfer', as: :sent, dependent: :nullify
  has_many :received_transfers, class_name: 'Transfer', as: :received, dependent: :nullify

  def get_transfers
    Transfer.where(sent: self).or(Transfer.where(received: self))
  end

  def get_history
    # " OR transfers.received_type = 'Company' AND transfers.received_id = companies.id"
    # можно добавить эту строку, если нам нужно видеть и приходящие транзакции
    self.get_transfers
        .joins("INNER JOIN companies ON transfers.sent_type = 'Company' AND transfers.sent_id = companies.id")
        .select('transfers.id', 'companies.name', 'transfers.value', 'transfers.created_at')
        .where("companies.id = #{self.id}")
  end

end
