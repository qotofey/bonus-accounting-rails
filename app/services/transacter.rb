class Transacter

  def self.transact(exchange, message = {})
    chan = channel.fanout("bonus-accounting.#{exchange}")
    chan.publish(message.to_json)
  end

  def self.channel
    @channel ||= connection.create_channel
  end

  def self.connection
    @connection ||= Bunny.new.tap do |c|
      c.start
    end
  end

end