# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.destroy_all
Company.destroy_all
Transfer.destroy_all

#Создаём пользователей
user = User.create(name: 'Тимофей', login: 'qotofey', password: '123123', created_at: Time.now - 10.days)
root = User.create(name: 'Root', login: 'root', password: '123123', created_at: Time.now - 10.days)
company = Company.create(name: "ООО РОГА И КОПЫТА", created_at: Time.now - 10.days)

#Выдаём им бонусы
Transfer.create(received: user, value: 2500, bonuses: 100, created_at: Time.now - 10.days)
Transfer.create(received: root, value: 2500, bonuses: 100, created_at: Time.now - 10.days)
Transfer.create(received: company, value: 25000, bonuses: 500, created_at: Time.now - 10.days)

#Транзакции между пользователями
Transfer.create(sent: user, received: root, value: 500, created_at: Time.now - 10.days)

#Проверим на DEADLOCK
thread1 = Thread.new do
  Transfer.create(sent: user, received: root, value: 290)
end
thread2 = Thread.new do
  Transfer.create(sent: root, received: user, value: 1000)
end

thread1.join
thread2.join

Transfer.create(sent: user, received: root, value: 200, created_at: Time.now - 7.days)
Transfer.create(sent: user, received: root, value: 500, created_at: Time.now - 6.days)
Transfer.create(sent: user, received: company, value: 290, created_at: Time.now - 5.days)
Transfer.create(sent: root, received: user, value: 1000, created_at: Time.now - 4.days)
Transfer.create(sent: user, received: root, value: 200, created_at: Time.now - 3.days)
Transfer.create(sent: root, received: user, value: 1000, created_at: Time.now - 2.days)
Transfer.create(sent: user, received: root, value: 200, created_at: Time.now - 1.days)
Transfer.create(sent: user, received: root, value: 200, created_at: Time.now)
Transfer.create(sent: company, received: root, value: 200, created_at: Time.now)
