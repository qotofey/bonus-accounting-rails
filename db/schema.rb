# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_29_165708) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.decimal "balance", precision: 12, scale: 2, default: "0.0"
    t.decimal "bonuses", precision: 12, scale: 2, default: "0.0"
    t.bigint "sent_transfers_count", default: 0
    t.bigint "received_transfers_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transfers", force: :cascade do |t|
    t.decimal "value", precision: 12, scale: 2, default: "0.0"
    t.decimal "bonuses", precision: 12, scale: 2, default: "0.0"
    t.string "sent_type"
    t.bigint "sent_id"
    t.string "received_type"
    t.bigint "received_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["received_type", "received_id"], name: "index_transfers_on_received_type_and_received_id"
    t.index ["sent_type", "sent_id"], name: "index_transfers_on_sent_type_and_sent_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "login"
    t.string "name"
    t.string "password_digest"
    t.decimal "balance", precision: 12, scale: 2, default: "0.0"
    t.decimal "bonuses", precision: 12, scale: 2, default: "0.0"
    t.bigint "sent_transfers_count", default: 0
    t.bigint "received_transfers_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
