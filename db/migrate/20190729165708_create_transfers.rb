class CreateTransfers < ActiveRecord::Migration[5.2]
  def change
    create_table :transfers do |t|
      t.decimal :value, precision: 12, scale: 2, default: 0
      t.decimal :bonuses, precision: 12, scale: 2, default: 0
      t.references :sent, polymorphic: true
      t.references :received, polymorphic: true

      t.timestamps
    end
  end
end
