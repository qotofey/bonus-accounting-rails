class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :login
      t.string :name
      t.string :password_digest
      t.decimal :balance, precision: 12, scale: 2, default: 0
      t.decimal :bonuses, precision: 12, scale: 2, default: 0

      t.bigint :sent_transfers_count, default: 0
      t.bigint :received_transfers_count, default: 0

      t.timestamps
    end
  end
end
